# bgbilling-with-maven

Репозиторий с примерами.

Структура проекта полностью сооветствует требованиям `Maven`, более подробно о Maven'e: https://maven.apache.org/index.html
Maven позволяется осуществлять автоматическую сборку артифкатов вплоть до деплоя на сервер и выполнения всех необходимых шагов для успешного деплоя,
копирование исходников, сборку архива... да все, что угодно. Почти.

Спасибо сообществу: теперь есть репозиторий с артифактами bgbilling'a. Работает в тестовом режиме. Для Подключения репозитория достаточно прописать его в pom.xml:
```xml
    <repositories>
        <repository>
            <id>bgbilling-community</id>
            <name>bgbilling-community</name>
            <url>http://91.204.234.250/repository/bgbilling/</url>
        </repository>
    </repositories>
```

Подключить зависимости bgbilling'а можно через зависимости:
```xml
    <dependency>
        <groupId>ru.bitel.bgbilling.kernel</groupId>
        <artifactId>kernel</artifactId>
        <version>${bgbilling.version}</version>
    </dependency>
```

`groupId` различается для модулей, плагинов, ядра:

* `ru.bitel.bgbilling.modules - модули`
* `ru.bitel.bgbilling.plugins - плагины`
* `ru.bitel.bgbilling.kernel - ядро`

Доступные версии:

* `6.0`
* `6.1`
* `6.2`
* `7.0`
* `7.1`
* `7.2`

Билды модулей не учитываются. Есть в планах сделать версии вплоть до билдов, но времени пока нет.

В модуле `dyncode` есть пример хендлера события до изменения параметра, который ограничивает изменение параметра по некоторым условиям:
`ContractParamChangeHandler`

К хендлеру написан тест роверяющий логику работы ограничений.
`ContractParamChangeHandlerTest`
