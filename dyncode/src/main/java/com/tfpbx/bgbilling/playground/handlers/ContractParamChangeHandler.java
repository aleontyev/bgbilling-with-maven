package com.tfpbx.bgbilling.playground.handlers;

import ru.bitel.bgbilling.kernel.event.events.ContractParamBeforeChangeEvent;
import ru.bitel.bgbilling.kernel.script.server.dev.EventScriptBase;
import ru.bitel.bgbilling.server.util.Setup;
import ru.bitel.common.sql.ConnectionSet;

import java.util.Arrays;

public class ContractParamChangeHandler extends EventScriptBase<ContractParamBeforeChangeEvent> {

    private static final Integer[] DISALLLOWED_FOR_CHANGE = new Integer[]{1,2,3,77};

    @Override
    public void onEvent(ContractParamBeforeChangeEvent event, Setup setup, ConnectionSet set) throws Exception {
        int paramId = event.getParamId();
        Object value = event.getValue();

        String error = null;

        // проверка на запрет изменения
        if (Arrays.asList(DISALLLOWED_FOR_CHANGE).contains(paramId)) {
            error = "Параметр запрещено изменять";
        }

        // проверка на наличие значения и минимальной длины
        if(value == null || String.valueOf(value).length() < 5) {
            error = "Недопустимое новое значение для параметра: " + String.valueOf(value);
        }

        if (error != null) {
            event.setError(error);
        }
    }
}
