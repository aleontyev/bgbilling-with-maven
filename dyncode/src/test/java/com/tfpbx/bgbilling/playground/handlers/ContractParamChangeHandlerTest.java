package com.tfpbx.bgbilling.playground.handlers;

import org.junit.Test;
import ru.bitel.bgbilling.kernel.event.events.ContractParamBeforeChangeEvent;

import static org.junit.Assert.*;

public class ContractParamChangeHandlerTest {

    private ContractParamChangeHandler contractParamChangeHandler = new ContractParamChangeHandler();


    @Test
    public void onEvent() throws Exception {

        ContractParamBeforeChangeEvent goodEvent = new ContractParamBeforeChangeEvent(0, 2, 100, "right string");
        contractParamChangeHandler.onEvent(goodEvent, null, null);
        assertNull("Ошибки быть не должно", goodEvent.getError());

        ContractParamBeforeChangeEvent restrictedEvent = new ContractParamBeforeChangeEvent(0, 2, 1, "right string");
        contractParamChangeHandler.onEvent(restrictedEvent, null, null);
        assertEquals("Изменение параметра должно быть запрещено", "Параметр запрещено изменять", restrictedEvent.getError());

        ContractParamBeforeChangeEvent badValueEvent = new ContractParamBeforeChangeEvent(0, 2, 100, "be");
        contractParamChangeHandler.onEvent(badValueEvent, null, null);
        assertEquals("Дожно быть сообщение о невалидном значении", "Недопустимое новое значение для параметра: be", badValueEvent.getError());

    }
}